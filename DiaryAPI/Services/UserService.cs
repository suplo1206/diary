﻿using DiaryAPI.Models;
using DiaryAPI.Models.RequestModels;
using DiaryAPI.Models.ResponseModels;
using DiaryAPI.Utils.AppSettings;
using DiaryAPI.Utils.Response;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DiaryAPI.Services
{
    public interface IUserService
    {
        Task<Response> Authenticate(RequestUser model);
    }

    public class UserService : IUserService
    {
        private DiaryContext context;
        private AppSettings appSettings;
        public UserService(DiaryContext context, IOptions<AppSettings> appSettings)
        {
            this.context = context;
            this.appSettings = appSettings.Value;
        }
        public async Task<Response> Authenticate(RequestUser model)
        {
            var user = await context.Users.FirstOrDefaultAsync(x => x.UserName.Equals(model.Username) && x.Password.Equals(model.Password));
            var response = new AuthenticateResponse();
            if (user != null)
            {
                response.AuthenticateSuccess();
                var responseUser = new ResponseUser { FullName = user.FullName, Token = GenerateToken(user) };
                response.Data = responseUser;
            }
            else
            {
                response.AuthenticateFails();
            }
            return response;
        }

        protected string GenerateToken(User user)
        {
            var handler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.SecretKey);
            SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.UserId.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = handler.CreateToken(descriptor);
            return handler.WriteToken(token);
        }
    }
}
