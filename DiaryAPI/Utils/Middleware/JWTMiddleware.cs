﻿using DiaryAPI.Models;
using DiaryAPI.Services;
using DiaryAPI.Utils.AppSettings;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiaryAPI.Utils.Middleware
{
    public class JWTMiddleware
    {
        private readonly RequestDelegate _next;
        private AppSettings.AppSettings appSettings;
        public JWTMiddleware(RequestDelegate next, IOptions<AppSettings.AppSettings> options)
        {
            _next = next;
            appSettings = options.Value;
        }

        public async Task InvokeAsync(HttpContext context, DiaryContext db)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            if (token != null)
                ValidateToken(token, context, db);
            await _next(context);
        }

        private void ValidateToken(string token, HttpContext context, DiaryContext db)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(appSettings.SecretKey);
                handler.ValidateToken(token, new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key)

                }, out SecurityToken validatedToken);
                var id = (validatedToken as JwtSecurityToken).Claims.First(x => x.Type == "id").Value;
                User user = db.Users.Find(id);
                context.Items["User"] = user;
            }
            catch (Exception)
            {
            }
        }

    }
}
