﻿using DiaryAPI.Models;
using DiaryAPI.Utils.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiaryAPI.Utils.Attributes
{
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public string[] Roles { get; set; }
        public AuthorizeAttribute()
        {
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var action = context.ActionDescriptor.RouteValues;
            var user = context.HttpContext.Items["User"] as User;
            if (user == null)
            {
                AuthenticateResponse response = new AuthenticateResponse();
                response.AuthenticateFails();
                context.Result = new ObjectResult(response) { StatusCode = (int)response.StatusCode};
            }

            //bool checkRole = false;
            //foreach (var roleOfAction in Roles)
            //{
               
            //    foreach (var roleOfUser in user.UserRoles)
            //    {
            //        if (roleOfAction == roleOfUser.Role.RoleName)
            //        {
            //            checkRole = true;
            //            break;
            //        }
            //    }
            //}
            //if (!checkRole)
            //{
            //    AuthenticateResponse response = new AuthenticateResponse();
            //    response.StatusCode = StatusCode.Forbidden;
            //    response.Message = "User don't have Permission to access";
            //    context.Result = new ObjectResult(response) { StatusCode = (int)response.StatusCode };
            //}
            
        }
    }
}
