﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiaryAPI.Utils.Response
{
    public enum StatusCode
    {
        OK = 200,
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        MethodNotAllowed = 405
    }
    public abstract class Response
    {
        public StatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public Response() { }
        public Response(StatusCode statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }
    }

    public class AuthenticateResponse : Response
    {
        public object Data { get; set; }
        public AuthenticateResponse(){ }
        public AuthenticateResponse(object data) {
            Data = data;
        }
        public AuthenticateResponse(StatusCode statusCode, string message) : base(statusCode, message){ }
        public void AuthenticateSuccess()
        {
            StatusCode = StatusCode.OK;
            Message ??= "Authorize Success"; 
        }
        public void AuthenticateFails()
        {
            StatusCode = StatusCode.Unauthorized;
            Message ??= "Unauthorize";
            Data = null;
        }
    }
    public class SuccessResponse : Response
    {

    }
}
