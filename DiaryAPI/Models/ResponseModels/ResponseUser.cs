﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiaryAPI.Models.ResponseModels
{
    public class ResponseUser
    {
        public string FullName { get; set; }
        public string Token { get; set; }
    }
}
