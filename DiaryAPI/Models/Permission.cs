﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DiaryAPI.Models
{
    public partial class Permission
    {
        public Permission()
        {
            RolePermissions = new HashSet<RolePermission>();
        }

        public string PermissionId { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }

        public virtual ICollection<RolePermission> RolePermissions { get; set; }
    }
}
