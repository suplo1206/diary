﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DiaryAPI.Models
{
    public partial class Category
    {
        public Category()
        {
            Posts = new HashSet<Post>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
        public bool? Status { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}
