﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DiaryAPI.Models
{
    public partial class Post
    {
        public int PostId { get; set; }
        public int? CategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string AuthorIds { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedDates { get; set; }
        public bool? Status { get; set; }

        public virtual Category Category { get; set; }
    }
}
