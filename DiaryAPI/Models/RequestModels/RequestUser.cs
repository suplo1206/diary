﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiaryAPI.Models.RequestModels
{
    public class RequestUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
