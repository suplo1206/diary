﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DiaryAPI.Models
{
    public partial class RolePermission
    {
        public string RoleId { get; set; }
        public string PermissionId { get; set; }

        public virtual Permission Permission { get; set; }
        public virtual Role Role { get; set; }
    }
}
