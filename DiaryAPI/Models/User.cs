﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

#nullable disable

namespace DiaryAPI.Models
{
    public partial class User
    {
        public User()
        {
            UserRoles = new HashSet<UserRole>();
        }

        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public DateTime? RegistrationTime { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        [JsonIgnore]
        public string Password { get; set; }
        public bool? Status { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
